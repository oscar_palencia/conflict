public class Square{
    private int side;
public Square(int side){
        this.side = side;
    }
public int getSide(){
    return this.side;
}
public int getArea(){
    return this.side*this.side;
}
public String toString(){
    return "This square has an area of "+getArea()+" and sides of size "+getSide();
}
}