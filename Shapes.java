public class Shapes{
    public static void main(String[] args){
        //triangle
        Triangle t = new Triangle(5, 4, 6, 8);
        System.out.println(t.toString());
        System.out.println(t.getArea());

        //square
        Square s = new Square(3);
        System.out.println(s.toString());
    }
}