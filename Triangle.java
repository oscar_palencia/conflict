public class Triangle {
    private double height;
    private double base;
    private double sideOne;
    private double sideTwo;

    public Triangle(double h, double b, double s1, double s2) {
        this.height = h;
        this.base = b;
        this.sideOne = s1;
        this.sideTwo = s2;
    }

    public double getHeight() {
        return this.height;
    }

    public double getBase() {
        return this.base;
    }

    public double getSideOne() {
        return this.sideOne;
    }

    public double getSideTwo() {
        return this.sideTwo;
    }

    public double getArea() {
        return (this.height * this.base) / 2;
    }

    public String toString() {
        return "height: "+this.height+", base: "+this.base+", side one: "+this.sideOne+", side two: "+this.sideTwo;
    }
}
